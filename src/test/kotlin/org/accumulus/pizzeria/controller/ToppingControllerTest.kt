package org.accumulus.pizzeria.controller

import org.accumulus.pizzeria.dto.request.CreateToppingsDto
import org.accumulus.pizzeria.service.ToppingService
import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.core.Is.`is`
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.mockito.Mockito.verify
import org.mockito.kotlin.argumentCaptor
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*

@WebMvcTest(ToppingController::class)
class ToppingControllerTest(@Autowired val mvc: MockMvc) {

    @MockBean
    lateinit var toppingService: ToppingService

    @Test
    fun `when body is empty then response Invalid payload`() {
        mvc.perform(post("/toppings").contentType(APPLICATION_JSON))
            .andExpect(status().isBadRequest)
            .andExpect(jsonPath("$.error", `is`("Invalid payload")))
    }

    @Test
    fun `when field email is missing then response Missing required field`() {
        mvc.perform(
            post("/toppings")
                .contentType(APPLICATION_JSON)
                .content(
                    """
                    {"toppings": [
                        {"title": "Fresh Basil"},
                        {"title": "Pesto"}
                    ]}
                    """.trimIndent()
                )
        )
            .andExpect(status().isBadRequest)
            .andExpect(jsonPath("$.error", `is`("Missing required field: email")))
    }

    @Test
    fun `when field toppings is missing then response Missing required field`() {
        mvc.perform(
            post("/toppings")
                .contentType(APPLICATION_JSON)
                .content("""{"email": "test@test.com"}""")
        )
            .andExpect(status().isBadRequest)
            .andExpect(jsonPath("$.error", `is`("Missing required field: toppings")))
    }

    @Test
    fun `when field email is invalid then response Invalid value in field`() {
        mvc.perform(
            post("/toppings")
                .contentType(APPLICATION_JSON)
                .content(
                    """
                    {"email": "invalidEmail",
                     "toppings": [
                        {"title": "Fresh Basil"},
                        {"title": "Pesto"}
                    ]}
                    """.trimIndent()
                )
        )
            .andExpect(status().isBadRequest)
            .andExpect(jsonPath("$.error", `is`("Invalid value in field: email")))
    }

    @Test
    fun `when field toppings title is invalid then response Invalid value in field`() {
        mvc.perform(
            post("/toppings")
                .contentType(APPLICATION_JSON)
                .content(
                    """
                    {"email": "test@test.com",
                     "toppings": [{"title": ""}]}
                    """.trimIndent()
                )
        )
            .andExpect(status().isBadRequest)
            .andExpect(jsonPath("$.error", `is`("Invalid value in field: toppings[].title")))
    }

    @Test
    fun `when request body is valid then return status 200`() {
        mvc.perform(
            post("/toppings")
                .contentType(APPLICATION_JSON)
                .content(
                    """
                    {"email": "test@test.com",
                     "toppings": [
                        {"title": "Fresh Basil"},
                        {"title": "Pesto"}
                    ]}
                    """.trimIndent()
                )
        )
            .andExpect(status().isCreated)

        val captor = argumentCaptor<CreateToppingsDto>()
        verify(toppingService).createToppingsForCustomer(captor.capture())
        val (email, toppings) = captor.firstValue
        assertThat(email).isEqualTo("test@test.com")
        assertThat(toppings).flatExtracting({ it.title }).containsExactlyInAnyOrder("Fresh Basil", "Pesto")
    }

}
