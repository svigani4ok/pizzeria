package org.accumulus.pizzeria.controller

import org.accumulus.pizzeria.dto.response.ToppingStatsDto
import org.accumulus.pizzeria.service.ToppingStatsService
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@WebMvcTest(ToppingStatsController::class)
class ToppingStatsControllerTest(@Autowired val mvc: MockMvc) {

    @MockBean
    lateinit var toppingStatsService: ToppingStatsService

    @Test
    fun `when get stats then return toppings stats`() {
        `when`(toppingStatsService.getToppingStats()).thenReturn(
            setOf(
                ToppingStatsDto("Fresh Basil", 3),
                ToppingStatsDto("Pesto", 5)
            )
        )
        mvc.perform(get("/toppings/stats"))
            .andExpect(status().isOk)
            .andExpect(
                content().json(
                    """
                [
                  {
                    "toppingTitle": "Fresh Basil",
                    "numberOfUniqueCustomers": 3
                  }, {
                    "toppingTitle": "Pesto",
                    "numberOfUniqueCustomers": 5
                  }
                ]
            """.trimIndent()
                )
            )
    }

}
