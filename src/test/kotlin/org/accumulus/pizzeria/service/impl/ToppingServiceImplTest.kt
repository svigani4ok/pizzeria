package org.accumulus.pizzeria.service.impl

import org.accumulus.pizzeria.dto.request.CreateToppingsDto
import org.accumulus.pizzeria.model.Customer
import org.accumulus.pizzeria.repository.CustomerRepository
import org.accumulus.pizzeria.service.mapper.CustomerMapper
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.jupiter.MockitoExtension

@ExtendWith(MockitoExtension::class)
class ToppingServiceImplTest {

    @Mock
    lateinit var customerMapper: CustomerMapper

    @Mock
    lateinit var customerRepository: CustomerRepository

    @InjectMocks
    lateinit var toppingService: ToppingServiceImpl

    @Test
    fun `when customer doesn't exist then save new one with toppings`() {
        val toppingsDto = CreateToppingsDto("email", setOf())
        val customer = Customer("email", setOf())
        `when`(customerMapper.toModel(toppingsDto)).thenReturn(customer)
        `when`(customerRepository.findByEmail("email")).thenReturn(null)

        toppingService.createToppingsForCustomer(toppingsDto)

        verify(customerRepository).save(customer)
    }

    @Test
    fun `when customer exists then update it's toppings`() {
        val toppingsDto = CreateToppingsDto("email", setOf())
        val customer = Customer("email", setOf())
        val savedCustomer = Customer("email", setOf())
        `when`(customerMapper.toModel(toppingsDto)).thenReturn(customer)
        `when`(customerRepository.findByEmail("email")).thenReturn(savedCustomer)

        toppingService.createToppingsForCustomer(toppingsDto)

        verify(customerRepository).save(savedCustomer)
    }
}
