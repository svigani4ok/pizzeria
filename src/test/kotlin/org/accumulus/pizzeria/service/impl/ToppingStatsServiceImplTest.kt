package org.accumulus.pizzeria.service.impl

import org.accumulus.pizzeria.dto.response.ToppingStatsDto
import org.accumulus.pizzeria.model.ToppingStats
import org.accumulus.pizzeria.repository.ToppingRepository
import org.accumulus.pizzeria.service.mapper.ToppingStatsMapper
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.verifyNoInteractions
import org.mockito.Mockito.`when`
import org.mockito.junit.jupiter.MockitoExtension

@ExtendWith(MockitoExtension::class)
class ToppingStatsServiceImplTest {

    @Mock
    lateinit var toppingStatsMapper: ToppingStatsMapper

    @Mock
    lateinit var toppingRepository: ToppingRepository

    @InjectMocks
    lateinit var toppingStatsService: ToppingStatsServiceImpl

    @Test
    fun `when toppings not found then return empty set`() {
        `when`(toppingRepository.findToppingStats()).thenReturn(setOf())

        val result = toppingStatsService.getToppingStats()

        assertThat(result).isEmpty()
        verifyNoInteractions(toppingStatsMapper)
    }

    @Test
    fun `when toppings found then map it to dto and return it`() {
        val toppingStats = object : ToppingStats {
            override fun getTitle() = "title"
            override fun getCount() = 5
        }
        `when`(toppingRepository.findToppingStats()).thenReturn(setOf(toppingStats))
        val toppingStatsDto = ToppingStatsDto("title", 5)
        `when`(toppingStatsMapper.toDto(toppingStats)).thenReturn(toppingStatsDto)

        val result = toppingStatsService.getToppingStats()

        assertThat(result).containsExactly(toppingStatsDto)
    }

}
