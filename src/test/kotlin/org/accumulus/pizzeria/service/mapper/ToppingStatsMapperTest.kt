package org.accumulus.pizzeria.service.mapper

import org.accumulus.pizzeria.model.ToppingStats
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class ToppingStatsMapperTest {

    private val toppingStatsMapper = ToppingStatsMapperImpl()

    @Test
    fun `test toDto method`() {
        val toppingStats = object : ToppingStats {
            override fun getTitle() = "title"
            override fun getCount() = 5
        }

        val result = toppingStatsMapper.toDto(toppingStats)

        assertThat(result.toppingTitle).isEqualTo("title")
        assertThat(result.numberOfUniqueCustomers).isEqualTo(5)
    }

}
