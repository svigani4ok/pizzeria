package org.accumulus.pizzeria.service.mapper

import org.accumulus.pizzeria.dto.request.CreateToppingsDto
import org.accumulus.pizzeria.dto.request.Topping
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class CustomerMapperTest {

    private val customerMapper = CustomerMapperImpl()

    @Test
    fun `test toModel method`() {
        val toppingDto = CreateToppingsDto("email", setOf(Topping("top1"), Topping("top2")))

        val result = customerMapper.toModel(toppingDto)

        assertThat(result.email).isEqualTo("email")
        assertThat(result.toppings)
            .flatExtracting({ it.title })
            .containsExactlyInAnyOrder("top1", "top2")
    }

}
