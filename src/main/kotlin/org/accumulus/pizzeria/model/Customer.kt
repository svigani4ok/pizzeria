package org.accumulus.pizzeria.model

import jakarta.persistence.CascadeType.*
import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType.IDENTITY
import jakarta.persistence.Id
import jakarta.persistence.OneToMany

@Entity
class Customer(
    var email: String,
    @OneToMany(cascade = [PERSIST, MERGE, REFRESH])
    var toppings: Set<Topping> = mutableSetOf()
) {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    var id: Long = 0
}
