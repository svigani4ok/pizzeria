package org.accumulus.pizzeria.model

interface ToppingStats {
    fun getTitle(): String
    fun getCount(): Int
}
