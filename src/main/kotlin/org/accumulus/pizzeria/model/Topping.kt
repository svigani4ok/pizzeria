package org.accumulus.pizzeria.model

import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType.IDENTITY
import jakarta.persistence.Id
import org.hibernate.annotations.SQLInsert

@Entity
@SQLInsert(
    sql = """
        insert into pizzeria.topping (title) 
        values (?) 
        on conflict (title) 
        do update set title = excluded.title 
        returning id
"""
)
class Topping(
    var title: String
) {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    var id: Int = 0
}
