package org.accumulus.pizzeria

import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import org.accumulus.pizzeria.dto.request.CreateToppingsDto
import org.accumulus.pizzeria.dto.request.Topping
import org.accumulus.pizzeria.service.ToppingService
import org.springframework.context.annotation.Profile
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import java.time.Instant
import kotlin.random.Random

val EMAILS = setOf("@gmail.com", "@protonmail.com", "@outlook.com", "@yandex.com", "@icloud.com")
val NAMES = setOf(
    "Nuria", "Prudencio", "Lizbeth", "Hubert", "Pavel",
    "Claudio", "Edward", "Dagmær", "Isis", "Dobroslava"
)
val TOPPINGS = setOf(
    "Pepperoni", "Mushrooms", "Sausage", "Onions", "Bacon",
    "Extra Cheese", "Peppers", "Black Olives", "Chicken", "Pineapple",
    "Spinach", "Fresh Basil", "Ham", "Pesto", "Beef"
)

@Profile("generate-data")
@Component
private class GenerateDataApplicationListener(
    val toppingService: ToppingService,
) {

    private val random = Random(Instant.now().epochSecond)
    private val log = KotlinLogging.logger {}

    @EventListener
    fun handleContextStart(event: ContextRefreshedEvent) = runBlocking {
        log.info { "=== Generate Data ===" }
        repeat(random.nextInt(5, 15)) {
            launch {
                val toppingsDto = CreateToppingsDto(generateEmail(), generateToppings())
                log.info { "Generate ${toppingsDto.toppings.size} toppings for ${toppingsDto.email}" }
                toppingService.createToppingsForCustomer(toppingsDto)
            }
        }
    }

    private fun generateToppings(): Set<Topping> {
        val limit = random.nextInt(2, 10)
        return (1..limit).map { Topping(TOPPINGS.random()) }.toSet()
    }

    private fun generateEmail(): String = NAMES.random() + EMAILS.random()
}
