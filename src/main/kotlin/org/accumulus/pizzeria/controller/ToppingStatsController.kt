package org.accumulus.pizzeria.controller

import org.accumulus.pizzeria.dto.response.ToppingStatsDto
import org.accumulus.pizzeria.service.ToppingStatsService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/toppings/stats")
class ToppingStatsController(
    val toppingStatsService: ToppingStatsService
) {

    @GetMapping
    fun getToppingStats(): Set<ToppingStatsDto> = toppingStatsService.getToppingStats()

}
