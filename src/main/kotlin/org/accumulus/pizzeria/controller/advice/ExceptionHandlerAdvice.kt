package org.accumulus.pizzeria.controller.advice

import com.fasterxml.jackson.databind.JsonMappingException
import com.fasterxml.jackson.module.kotlin.MissingKotlinParameterException
import org.accumulus.pizzeria.controller.ToppingController
import org.accumulus.pizzeria.controller.advice.ErrorResponseDto.Companion.INVALID_PAYLOAD_ERROR_RESPONSE_DTO
import org.accumulus.pizzeria.controller.advice.ErrorResponseDto.Companion.invalidFieldValueErrorResponseDto
import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.badRequest
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

@ControllerAdvice(assignableTypes = [ToppingController::class])
class ExceptionHandlerAdvice {

    @ExceptionHandler
    fun handleHttpMessageNotReadableException(
        exception: HttpMessageNotReadableException
    ): ResponseEntity<ErrorResponseDto> {
        val cause = exception.cause
        val responseBody: ErrorResponseDto = if (cause is MissingKotlinParameterException) {
            cause.path
                .joinToString(".") { mapToFieldName(it) }
                .let(ErrorResponseDto::missingFieldErrorResponseDto)
        } else {
            INVALID_PAYLOAD_ERROR_RESPONSE_DTO
        }
        return badRequest().body(responseBody)
    }

    @ExceptionHandler
    fun handleMethodArgumentNotValidException(
        exception: MethodArgumentNotValidException
    ): ResponseEntity<ErrorResponseDto> {
        val responseBody = exception.fieldError?.let { invalidFieldValueErrorResponseDto(it.field) }
            ?: INVALID_PAYLOAD_ERROR_RESPONSE_DTO
        return badRequest().body(responseBody)
    }

    private fun mapToFieldName(it: JsonMappingException.Reference) =
        if (it.index > -1) "[${it.index}]" else it.fieldName

}

data class ErrorResponseDto(val error: String) {
    companion object {
        fun missingFieldErrorResponseDto(field: String) =
            ErrorResponseDto("Missing required field: $field")

        fun invalidFieldValueErrorResponseDto(field: String) =
            ErrorResponseDto("Invalid value in field: $field")

        val INVALID_PAYLOAD_ERROR_RESPONSE_DTO = ErrorResponseDto("Invalid payload")
    }
}
