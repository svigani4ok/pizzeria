package org.accumulus.pizzeria.controller

import jakarta.validation.Valid
import org.accumulus.pizzeria.dto.request.CreateToppingsDto
import org.accumulus.pizzeria.service.ToppingService
import org.springframework.http.HttpStatus.CREATED
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*

@Validated
@RestController
@RequestMapping("/toppings")
class ToppingController(
    val toppingService: ToppingService
) {

    @ResponseStatus(CREATED)
    @PostMapping
    fun createToppingList(@Valid @RequestBody toppingRequest: CreateToppingsDto) =
        toppingService.createToppingsForCustomer(toppingRequest)

}
