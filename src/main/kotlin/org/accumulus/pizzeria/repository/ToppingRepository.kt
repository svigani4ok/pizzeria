package org.accumulus.pizzeria.repository

import org.accumulus.pizzeria.model.Topping
import org.accumulus.pizzeria.model.ToppingStats
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface ToppingRepository : JpaRepository<Topping, Int> {

    @Query(
        nativeQuery = true,
        value = """
            select t.title as title, count(ct) as count from pizzeria.customer_toppings ct
            left join pizzeria.topping t on ct.toppings_id = t.id
            group by t.title
        """
    )
    fun findToppingStats(): Set<ToppingStats>
}
