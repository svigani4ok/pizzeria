package org.accumulus.pizzeria.repository

import org.accumulus.pizzeria.model.Customer
import org.springframework.data.jpa.repository.JpaRepository

interface CustomerRepository : JpaRepository<Customer, Long> {
    fun findByEmail(email: String): Customer?

}
