package org.accumulus.pizzeria.dto.request

import jakarta.validation.Valid
import jakarta.validation.constraints.Email
import jakarta.validation.constraints.NotEmpty
import jakarta.validation.constraints.NotNull

data class CreateToppingsDto(
    @field:NotNull @field:Email val email: String,
    @field:NotEmpty @field:Valid val toppings: Set<Topping>
)

data class Topping(@field:NotEmpty val title: String)
