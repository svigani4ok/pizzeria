package org.accumulus.pizzeria.dto.response

data class ToppingStatsDto(val toppingTitle: String, val numberOfUniqueCustomers: Int)
