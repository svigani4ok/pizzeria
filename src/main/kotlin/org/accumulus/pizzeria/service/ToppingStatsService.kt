package org.accumulus.pizzeria.service

import org.accumulus.pizzeria.dto.response.ToppingStatsDto

interface ToppingStatsService {

    fun getToppingStats(): Set<ToppingStatsDto>

}
