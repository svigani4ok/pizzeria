package org.accumulus.pizzeria.service.mapper

import org.accumulus.pizzeria.dto.response.ToppingStatsDto
import org.accumulus.pizzeria.model.ToppingStats
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.MappingConstants.ComponentModel.SPRING

@Mapper(componentModel = SPRING)
interface ToppingStatsMapper {

    @Mapping(target = "toppingTitle", source = "title")
    @Mapping(target = "numberOfUniqueCustomers", source = "count")
    fun toDto(toppingStats: ToppingStats): ToppingStatsDto
}
