package org.accumulus.pizzeria.service.mapper

import org.accumulus.pizzeria.dto.request.CreateToppingsDto
import org.accumulus.pizzeria.model.Customer
import org.mapstruct.Mapper
import org.mapstruct.MappingConstants.ComponentModel.SPRING
import org.mapstruct.ReportingPolicy.IGNORE

@Mapper(componentModel = SPRING, unmappedTargetPolicy = IGNORE)
interface CustomerMapper {

    fun toModel(toppingsDto: CreateToppingsDto): Customer
}
