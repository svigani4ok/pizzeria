package org.accumulus.pizzeria.service

import org.accumulus.pizzeria.dto.request.CreateToppingsDto

interface ToppingService {

    fun createToppingsForCustomer(toppingsDto: CreateToppingsDto)

}
