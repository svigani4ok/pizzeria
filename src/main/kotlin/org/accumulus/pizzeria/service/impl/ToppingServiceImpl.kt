package org.accumulus.pizzeria.service.impl

import org.accumulus.pizzeria.repository.CustomerRepository
import org.accumulus.pizzeria.dto.request.CreateToppingsDto
import org.accumulus.pizzeria.service.ToppingService
import org.accumulus.pizzeria.service.mapper.CustomerMapper
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class ToppingServiceImpl(
    val customerMapper: CustomerMapper,
    val customerRepository: CustomerRepository
) : ToppingService {

    @Transactional
    override fun createToppingsForCustomer(toppingsDto: CreateToppingsDto) {
        val newCustomer = customerMapper.toModel(toppingsDto)
        val customerToSave = customerRepository.findByEmail(newCustomer.email) ?: newCustomer
        customerToSave.toppings = newCustomer.toppings
        customerRepository.save(customerToSave)
    }
}
