package org.accumulus.pizzeria.service.impl

import org.accumulus.pizzeria.dto.response.ToppingStatsDto
import org.accumulus.pizzeria.repository.ToppingRepository
import org.accumulus.pizzeria.service.ToppingStatsService
import org.accumulus.pizzeria.service.mapper.ToppingStatsMapper
import org.springframework.stereotype.Service

@Service
class ToppingStatsServiceImpl(
    val toppingStatsMapper: ToppingStatsMapper,
    val toppingRepository: ToppingRepository
) : ToppingStatsService {

    override fun getToppingStats(): Set<ToppingStatsDto> =
        toppingRepository.findToppingStats()
            .map(toppingStatsMapper::toDto)
            .toSet()

}
