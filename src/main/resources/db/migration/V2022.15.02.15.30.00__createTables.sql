create table customer
(
    id    bigserial primary key,
    email text not null unique
);

create table topping
(
    id    serial primary key,
    title text not null unique
);

create table customer_toppings
(
    customer_id bigint not null references customer (id),
    toppings_id int    not null references topping (id)
);
